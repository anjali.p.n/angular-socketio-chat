import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'chat-speech-bubble',
  templateUrl: './speech-bubble.component.html',
  styleUrls: ['./speech-bubble.component.scss']
})
export class SpeechBubbleComponent implements OnInit {

  @Input() message : String;
  
  constructor() { }

  ngOnInit() {
  }

}

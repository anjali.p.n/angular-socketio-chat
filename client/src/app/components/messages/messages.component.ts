import { Component, OnInit } from '@angular/core';
import { ChatService } from 'src/app/services/chat.service';

@Component({
  selector: 'chat-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {

  title = 'client';
  message: string;
  messages: string[] = [];

  constructor(private chatService: ChatService) {

  }

  keyDownOnTextArea(event) {
    if(!this.message || this.message.trim() === '')
      return;
    if(event.shiftKey && event.key === 'Enter')
      return;
    if (event.key === 'Enter') {
      this.sendMessage();
    }
  }

  sendMessage() {
    this.chatService.sendMessage(this.message);
    this.message = '';
    console.log(this.messages);
  }

  ngOnInit() {
    this.chatService
      .getMessages()
      .subscribe((message: string) => {
        this.messages.push(message);
      });
  }


}

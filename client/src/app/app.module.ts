
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChatService } from './services/chat.service';
import { MessagesComponent } from './components/messages/messages.component';
import { FormsModule } from '@angular/forms';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCardModule} from '@angular/material';
import { SpeechBubbleComponent } from './components/speech-bubble/speech-bubble.component';


@NgModule({
  declarations: [
    AppComponent,
    MessagesComponent,
    SpeechBubbleComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    FormsModule,
  ],
  entryComponents: [SpeechBubbleComponent],
  providers: [ ChatService ],
  bootstrap: [AppComponent]
})
export class AppModule { }

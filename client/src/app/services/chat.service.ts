import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  private url = "http://localhost:3000";
  private socket;

  constructor() { 
    this.socket = io(this.url);
  }

  public sendMessage(message){ 
    this.socket.emit('toServer', message);
  }

  public getMessages = () => {
    return Observable.create((observer) => {
        this.socket.on('toClient', (message) => {
            observer.next(message);
        });
    });
}
}

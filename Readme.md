# Chat app using Angular and Socket.io

https://codingblast.com/chat-application-angular-socket-io/

## Run server

1. cd to the 'server' folder
2. Run the command `node index.js`

## Run client

1. cd to the 'client' folder
2. Run the following commands:

        npm i
        ng serve -o